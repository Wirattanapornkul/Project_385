<%-- 
    Document   : Register
    Created on : Apr 27, 2017, 11:33:36 PM
    Author     : w-u-t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ลงทะเบียนนักเรียน</title>
    </head>
    <body bgcolor = "#D3D3D3">
        <div>
            <h1>โครงการโอลิมปิกวิชาการ(สาขาคอมพิวเตอร์) ค่าย 2</h1>
            <table border="0">
                <tr>
                    <td>เลขบัตรประชาชน</td>
                    <td><input type="text" name="idCard" size="20"></td>
                </tr>   
            </table>
            <table style="width:100%">
                <tbody>
                     <tr>
                         <td>คำนำหน้า:                     
                            <select>
                                <option value="ด.ช.">ด.ช.</option>
                                <option value="ด.ญ.">ด.ญ.</option>
                            </select>
                         <td>ชื่อ: <input type="text" /></td>
                         <td>นามสกุล <input type="text" /></td><br><br>
                     <tr>
                     <tr>
                         <td>เพศ 
                            <select>
                                <option value="ชาย">ชาย</option>
                                <option value="หญิง">หญิง</option>
                            </select></td>
                            <td>ชื่อโรงเรียน: <input type="text" /></td>
                            <td>เบอร์โทรศัพท์มือถือ: <input type="text" /></td>
                     </tr>
                     <tr>
                         <td>
                            สถานะ: 
                                <div>
                                <from>
                                    <input type="radio" name="นักเรียนเข้าค่าย" value="นักเรียนเข้าค่าย"> นักเรียนเข้าค่าย<br>
                                    <input type="radio" name="นักเรียนขอใช้สิทธิสอบ" value="นักเรียนขอใช้สิทธิสอบ"> นักเรียนขอใช้สิทธิสอบ<br>
                                    <input type="radio" name="อาจารย์ผู้ดูแล" value="อาจารย์ผู้ดูแล"> อาจารย์ผู้ดูแล<br>
                                </from>
                                </div> 
                         </td>
                         <td>ชื่อนามสกุลผู้ปกครอง: <input type="text" /></td>
                         <td>เบอร์โทรผู้ปกครอง: <input type="text" /></td>
                     </tr>
                     <tr>
                         <td>Email: <input type="text" /></td>
                     </tr>
                </tbody>
            </table>
            <center><td><button type="add">เพิ่ม</button></td>
            <td><button type="back">ย้อนกลับ</button></td></center>
        </div>
        <hr>
        <div>
            จำนวนนักเรียนที่ลงทะเบียน
            <center>
                <table border="2" bgcolor="#9370DB">
                    <tr>
                        <th>เลขที่บัตรประชาชน</th>
                        <th>คำนำหน้า</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>เพศ</th>
                        <th>ชื่อโรงเรียน</th>
                        <th>ชื่อนามสกุลผู้ปกครอง</th>
                        <th>เบอร์โทรศัพท์ผู้ปกครอง</th>
                        <th>สถานะ</th>
                        <th>E-mail</th>
                        <th>File</th>
                    </tr>
                </table>               
            </center>
        </div>
    </body>
</html>
